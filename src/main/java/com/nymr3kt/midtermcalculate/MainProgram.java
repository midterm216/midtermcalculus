/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.midtermcalculate;

/**
 *
 * @author nymr3kt
 */
public class MainProgram {
    public static void main(String[] args) {
        Calculate area = new Calculate(0,0,0);     /* สร้างออบเจ็กต์ จากคลาส Calculate*/
        System.out.println("Area Triangle : " + area.area(5, 3));
        System.out.print("Area Circle : ");
        System.out.printf("%.2f",area.area(3) );
    }
    
}
