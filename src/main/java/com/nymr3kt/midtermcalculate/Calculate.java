/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.midtermcalculate;

/**
 *
 * @author nymr3kt
 */

/* การประกาศคลาสต้นแบบ*/ 
public class Calculate {
    
    /* การประกาศตัวแปร โดยกำหนดระดับ access modifier เป็น private
    เพื่อบอกว่าตัวแปรนี้ไม่สามารถให้คลาสอื่นเข้าถึงได้*/
    private int x ;
    private int y ;
    private float r ;
    /*สร้าง constructor เพื่อกำหนดค่าเริ่มต้นให้กับออบเจ็กต์ก่อนถูกเรียกใช้งาน จะไม่มีการคืนค่า */
    public Calculate(int x,int y ,float r){
        this.x =  x ;
        this.y = y ;
        this.r = r ;
    }
    /* สร้างเมธอดขึ้นเพื่อสร้างการทำงานภายในเมธอด 
    ซึ่งเมธอดนี้จะไม่มีการรับค่าพารามิเตอร์ แต่มีการคืนค่ากลับไปที่ตัวแปรที่ใช้เก็บค่า*/
    public int area(int x, int y) {
        return x*y ;
    }
    /* บรรทัดที่ 29 และ บรรทัดที่ 32 จะเห็นได้ว่ามีชื่อเมธอดเหมือนกันก็คือ area 
    แต่จะสังเกตได้ว่ามีแอตทริบิวต์และประเภทข้อมูลที่ไม่เหมือนกัน 
    ซึ่งเมธอดลักษณะนี้จะเรียกว่า Overloading Method */
    
    public float area(float r){
        return (float) ((22.0/7)*r*r) ;
    }
    
}
